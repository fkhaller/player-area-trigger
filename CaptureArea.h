#pragma once

#include "PlayerAreaTrigger.h"

class CCaptureAreaComponent : public CPlayerAreaTriggerComponent {

public:

	enum CaptureType {
		AMMO = 1 << 0,
		FUEL = 1 << 1,
		REINFORCMENTS = 1 << 2
	};


public:

	CCaptureAreaComponent() = default;
	virtual ~CCaptureAreaComponent() { }

	virtual void Initialize() override;
	virtual void ProcessEvent(SEntityEvent& event) override;

	void SetCaptureType(int16 flags) { m_fCaptureTypeFlags |= flags; };
	int8 GetCaptureTypeFlags() { return m_fCaptureTypeFlags; };

	bool GetIsCaptured(){ return m_bCaptured; };

protected:
	
	int8 m_fCaptureTypeFlags;

	bool m_bCaptured;
	bool m_bContested;
	int m_iCaptureTicker;
	

	int m_CapturingTeam;

	int m_iGermanCount;
	int m_iRussianCount;
};