/* **************************************************

Author : Forrest Haller
Project : WWII Shooter
Purpose : Create an area trigger as a base class for capture system

**************************************************** */

#pragma once

#include <array>
#include <numeric>

#include <CryEntitySystem/IEntityComponent.h>
#include "Player.h"

#include <CryGame/IGameFramework.h>

#define DEBUG 1

class CPlayerAreaTriggerComponent : public IEntityComponent {

public:
	struct Cylinder {
		Vec3 position;
		f32 radius;
		f32 height;

		Cylinder() {
			position = Vec3(0, 0, 0);
			radius = 0;
			height = 0;
		}
		
		bool IsContainPoint(const Vec3 &pos)
		{
			CRY_MATH_ASSERT(position.IsValid());
			if (
				sqrt(sqr(pos.x - position.x) + sqr(pos.y - position.y)) < radius
				&& pos.z > position.z && pos.z < position.z + height
				) return true;
			else
				return false;
		}

	};

public:
	CPlayerAreaTriggerComponent() = default;
	virtual ~CPlayerAreaTriggerComponent() {}

	//IEntityComponent
	virtual void Initialize() override;
	virtual void OnShutDown() override;

	virtual uint64 GetEventMask() const override;
	virtual void ProcessEvent(SEntityEvent& event) override;
	//~IEntityComponent

	static void ReflectType(Schematyc::CTypeDesc<CPlayerAreaTriggerComponent>& desc)
	{
		desc.SetGUID("{63F4C0C6-32AF-4ACB-8FB0-57D45DD59873}"_cry_guid);
	}

	virtual EEntityAreaType GetAreaType() const { return m_AreaType; };

	AABB* GetBox() { return m_pAreaBox; };
	Cylinder* GetCylinder() { return m_pAreaCylinder; };

	void SetBox(AABB* box);
	void SetCylinder(Vec3 position, f32 radius, f32 height);


protected:

	void AddPlayer(CPlayerComponent* component);
	void RemovePlayer(CPlayerComponent* component);
	void RemovePlayers();

	int GetNumberOfEntitiesInArea() const { return m_iPlayersInArea; };

	EEntityAreaType m_AreaType;
	AABB* m_pAreaBox;
	Cylinder* m_pAreaCylinder;


	CPlayerComponent** m_pPlayersInArea;

	int m_iPlayersInArea;

	void Render() const;
};