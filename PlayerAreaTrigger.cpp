#include "StdAfx.h"
#include "PlayerAreaTrigger.h"

#define MAX_PLAYERS 16

void CPlayerAreaTriggerComponent::Initialize() {

	m_pAreaBox = (AABB*)malloc(sizeof(AABB));
	m_pAreaCylinder = (Cylinder*)malloc(sizeof(Cylinder));
	m_pPlayersInArea = (CPlayerComponent**)malloc(MAX_PLAYERS * sizeof(CPlayerComponent*));
	for (int i = 0; i < MAX_PLAYERS; i++) {
		m_pPlayersInArea[i] = (CPlayerComponent*)malloc(sizeof(CPlayerComponent));
	}

	*m_pAreaBox = AABB(Vec3(0,0,0),Vec3(0,0,0));
	*m_pAreaCylinder = Cylinder();
	RemovePlayers();

	m_iPlayersInArea = 0;

#if DEBUG
	//Test Code
	CPlayerComponent* testPlayer1 = &CPlayerComponent();
	CPlayerComponent* testPlayer2 = &CPlayerComponent();;
	CPlayerComponent* testPlayer3 = &CPlayerComponent();;

	SetCylinder(Vec3(60,60,32), 5, .1);

	AddPlayer(testPlayer1);
	AddPlayer(testPlayer2);
	AddPlayer(testPlayer2);
	RemovePlayer(testPlayer2);
	AddPlayer(testPlayer3);
	AddPlayer(testPlayer2);
	RemovePlayers();
	AddPlayer(testPlayer1);

	if (m_iPlayersInArea == 1) {
		CryLog("test success");
	}
	RemovePlayers();
#endif

}

void CPlayerAreaTriggerComponent::OnShutDown() {

	free(m_pAreaBox);
	free(m_pAreaCylinder);
	free(m_pPlayersInArea);

}

uint64 CPlayerAreaTriggerComponent::GetEventMask() const
{
	return BIT64(ENTITY_EVENT_UPDATE);
}

void CPlayerAreaTriggerComponent::ProcessEvent(SEntityEvent& event) {

	switch (event.event)
	{
	case ENTITY_EVENT_UPDATE:
	{
		#if DEBUG
			Render();
		#endif

			//get all of the players in the area and add them to the list
			auto *pEntityIterator = gEnv->pEntitySystem->GetEntityIterator();
			pEntityIterator->MoveFirst();

			while (!pEntityIterator->IsEnd())
			{
				IEntity *pEntity = pEntityIterator->Next();

				if (CPlayerComponent* player = pEntity->GetComponent<CPlayerComponent>())
				{
					bool isInArea = false;

					switch (m_AreaType)
					{
						
						case EEntityAreaType::ENTITY_AREA_TYPE_BOX:
							if (m_pAreaBox->IsContainPoint(pEntity->GetPos())) {
								isInArea = true;
							}
						case EEntityAreaType::ENTITY_AREA_TYPE_SHAPE:
							if (m_pAreaCylinder->IsContainPoint(pEntity->GetPos())) {
								isInArea = true;
							}
					}

					if (isInArea) {
						AddPlayer(player);
					}
					else {
						RemovePlayer(player);
					}
					
					
				}
			}
			//

	}
	}
}

void CPlayerAreaTriggerComponent::AddPlayer(CPlayerComponent * component)
{

	for (int i = 0; i < MAX_PLAYERS; i++) {
		if (m_pPlayersInArea[i] == component) {
			break;
		}
		else if (!m_pPlayersInArea[i]) {

			m_pPlayersInArea[m_iPlayersInArea++] = component;

			#if DEBUG
				CryLog("add " + ToString(i));
			#endif
			break;
		}
	}

}

void CPlayerAreaTriggerComponent::RemovePlayer(CPlayerComponent * component)
{

	for (int i = 0; i < MAX_PLAYERS; i++) {
		if (m_pPlayersInArea[i] == component) {

			m_pPlayersInArea[i] = NULL;
			m_pPlayersInArea[i] = m_pPlayersInArea[m_iPlayersInArea--];

			#if DEBUG
				CryLog("remove " + ToString(m_iPlayersInArea));
			#endif
			break;
		}
	}

}

void CPlayerAreaTriggerComponent::RemovePlayers()
{
	for (int i = 0; i < MAX_PLAYERS; i++) {
		m_pPlayersInArea[i] = NULL;
		m_iPlayersInArea = 0;
	}
}

void CPlayerAreaTriggerComponent::Render() const
{


		IRenderAuxGeom* render = gEnv->pRenderer->GetIRenderAuxGeom();

		switch (m_AreaType)
		{

		case EEntityAreaType::ENTITY_AREA_TYPE_BOX:
			render->DrawAABB(*m_pAreaBox, false, ColorB(Col_Cyan), eBBD_Faceted);
		case EEntityAreaType::ENTITY_AREA_TYPE_SHAPE:
			render->DrawCylinder(m_pAreaCylinder->position, Vec3(0, 0, 1), m_pAreaCylinder->radius, m_pAreaCylinder->height, ColorB(Col_Red));
		}
		
}

void CPlayerAreaTriggerComponent::SetBox(AABB * box)
{
	m_pAreaBox = box;
	m_AreaType = EEntityAreaType::ENTITY_AREA_TYPE_BOX;
	RemovePlayers();
}

void CPlayerAreaTriggerComponent::SetCylinder(Vec3 position, f32 radius, f32 height)
{

	m_pAreaCylinder->position = position;
	m_pAreaCylinder->radius = radius;
	m_pAreaCylinder->height = height;
	m_AreaType = EEntityAreaType::ENTITY_AREA_TYPE_SHAPE;
	RemovePlayers();

}
