#include "CaptureArea.h"
#include "Player.h"

void CCaptureAreaComponent::Initialize()
{
	__super::Initialize();
	m_iCaptureTicker = 0;
}

void CCaptureAreaComponent::ProcessEvent(SEntityEvent& event)
{

	__super::ProcessEvent(event);

	switch (event.event)
	{
	case ENTITY_EVENT_UPDATE:
	{
		m_iGermanCount = 0;
		m_iRussianCount = 0;


		//decide capturing team
		if (m_iCaptureTicker <= 0) {

			//make sure if it drops below 0 it gets reset
			m_iCaptureTicker = 0;

			//first player in area
			if (m_iPlayersInArea == 1)
				m_CapturingTeam = m_pPlayersInArea[0]->GetTeam();
			//if the area has multiple players and it reached 0 then the point must have changed hands
			else if (m_iPlayersInArea > 1) {
				if (m_CapturingTeam == CPlayerComponent::CPlayerComponent::RUSSIAN) {
					m_CapturingTeam = CPlayerComponent::GERMAN;
				}
				else if (m_CapturingTeam == CPlayerComponent::GERMAN) {
					m_CapturingTeam = CPlayerComponent::RUSSIAN;
				}
			}

		}

		//find out how many of each faction are in the area
		for (int i = 0; i < m_iPlayersInArea; i++) {
			if (m_pPlayersInArea[i]->GetTeam() == CPlayerComponent::GERMAN) {
				m_iGermanCount++;
			}
			else if (m_pPlayersInArea[i]->GetTeam() == CPlayerComponent::RUSSIAN) {
				m_iRussianCount++;
			}
		}
		if (m_iGermanCount == 0 && m_iRussianCount == 0) {
			m_iCaptureTicker--;
		}

		//tally up capturing team or bring down if no one is fighting for it
		if (m_CapturingTeam == CPlayerComponent::GERMAN) {
			m_iCaptureTicker += m_iGermanCount;
			m_iCaptureTicker -= m_iRussianCount;
		}
		else if (m_CapturingTeam == CPlayerComponent::RUSSIAN) {
			m_iCaptureTicker += m_iRussianCount;
			m_iCaptureTicker -= m_iGermanCount;
		}

		//you may not capture a point until you ave no enemies on it but you can 
		//over cap it to prolong the time it takes to be neutralized
		m_bContested = false;
		if (m_iGermanCount != 0 && m_iRussianCount != 0) {
			m_bContested = true;
		}

		if (!m_bCaptured && !m_bContested) {
			if (m_iCaptureTicker >= 3000) {
				m_iCaptureTicker = 3000;
				m_bCaptured = true;
			}
#if DEBUG
			else if (m_iCaptureTicker > 0) {
				CryLog(ToString(m_iCaptureTicker));
			}
			if (m_bCaptured) {
				CryLog("captured");
			}
#endif

		}
	}
	}
}